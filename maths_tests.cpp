#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "maths.h"
#include "maths.cpp"

TEST_CASE("test primes", "[is_prime]")
{
  REQUIRE(is_prime(2) == true);
  REQUIRE(is_prime(3) == true);
  REQUIRE(is_prime(4) == false);
  REQUIRE(is_prime(5) == true);

  REQUIRE(absolute(-6) == 6);
  REQUIRE(absolute(-2) == 2);

  REQUIRE(power(2, 3) == 8);
  REQUIRE(power(5, 3) == 125);
}
